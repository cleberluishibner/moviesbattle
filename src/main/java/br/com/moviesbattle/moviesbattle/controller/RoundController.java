package br.com.moviesbattle.moviesbattle.controller;

import br.com.moviesbattle.moviesbattle.dto.AnswerDto;
import br.com.moviesbattle.moviesbattle.dto.RoundDto;
import br.com.moviesbattle.moviesbattle.mapper.RoundMapper;
import br.com.moviesbattle.moviesbattle.service.RoundService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("round")
public class RoundController {

    private final RoundService roundService;
    private final RoundMapper roundMapper;

    public RoundController(final RoundService roundService,
                           final RoundMapper roundMapper) {
        this.roundService = roundService;
        this.roundMapper = roundMapper;
    }

    @GetMapping
    @ApiOperation(value = "Return round")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "The user has no game started"),
            @ApiResponse(code = 400, message = "You lost, finish this game and start a new one")
    })
    public ResponseEntity<RoundDto> round() {
        final var round = roundService.getRound();
        return ResponseEntity.ok(roundMapper.toDto(round));
    }

    @PostMapping
    @ApiOperation(value = "Send answer")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Return true if the answer is correct or false if is wrong"),
            @ApiResponse(code = 400, message = "The user has no game started"),
            @ApiResponse(code = 400, message = "Cant find current round, please request another")
    })
    public ResponseEntity<Boolean> answer(@Valid @RequestBody AnswerDto answer) {
        return ResponseEntity.ok(roundService.answer(answer.getSelectedMovieImdbId()));
    }
}
