package br.com.moviesbattle.moviesbattle.controller;

import br.com.moviesbattle.moviesbattle.dto.RankingDto;
import br.com.moviesbattle.moviesbattle.mapper.RakingMapper;
import br.com.moviesbattle.moviesbattle.service.RankingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("ranking")
public class RankingController {

    private final RankingService rankingService;
    private final RakingMapper rakingMapper;

    public RankingController(final RankingService rankingService,
                             final RakingMapper rakingMapper) {
        this.rankingService = rankingService;
        this.rakingMapper = rakingMapper;
    }

    @GetMapping
    @ApiOperation(value = "Game ranking")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 204, message = "No Content")})
    public ResponseEntity<List<RankingDto>> findAll() {
        var rankings = rankingService.findAllRanking();
        if (rankings == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(rakingMapper.toDto(rankings));
    }
}
