package br.com.moviesbattle.moviesbattle.controller;

import br.com.moviesbattle.moviesbattle.dto.UserDto;
import br.com.moviesbattle.moviesbattle.mapper.UserMapper;
import br.com.moviesbattle.moviesbattle.service.LoginService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("login")
public class LoginController {

    private final LoginService loginService;
    private final UserMapper userMapper;

    public LoginController(final LoginService loginService,
                           final UserMapper userMapper) {
        this.loginService = loginService;
        this.userMapper = userMapper;
    }

    @PostMapping
    @ApiOperation(value = "Login in the game")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "Invalid login or password")})
    public ResponseEntity<String> login(@Valid @RequestBody final UserDto user) {
        final String token = loginService.login(userMapper.toEntity(user));
        return ResponseEntity.ok(token);
    }
}