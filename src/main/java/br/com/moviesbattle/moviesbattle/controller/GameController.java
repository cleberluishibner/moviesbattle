package br.com.moviesbattle.moviesbattle.controller;

import br.com.moviesbattle.moviesbattle.dto.GameDto;
import br.com.moviesbattle.moviesbattle.mapper.GameMapper;
import br.com.moviesbattle.moviesbattle.service.GameService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("game")
public class GameController {

    private final GameService gameService;
    private final GameMapper gameMapper;

    public GameController(final GameService gameService,
                          final GameMapper gameMapper) {
        this.gameService = gameService;
        this.gameMapper = gameMapper;
    }

    @GetMapping
    @ApiOperation(value = "Return to the game of the logged user")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 204, message = "No Content")})
    public ResponseEntity<GameDto> getGameUserLogged() {
        var game = gameService.getGameUserLogged();
        if (game == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(gameMapper.toDto(game));
    }

    @PostMapping("start")
    @ApiOperation(value = "Start a new game")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "The user already has a game started")})
    public void start() {
        gameService.start();
    }

    @PostMapping("finish")
    @ApiOperation(value = "Finish a game")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 400, message = "The user has no game started")})
    public void finish() {
        gameService.finish();
    }
}
