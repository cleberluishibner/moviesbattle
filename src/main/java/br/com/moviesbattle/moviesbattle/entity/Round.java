package br.com.moviesbattle.moviesbattle.entity;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne
    private Game game;

    @NotBlank
    private String leftMovieImdbId;

    @NotBlank
    private String leftMovieTitle;

    @NotBlank
    private String rightMovieImdbId;

    @NotBlank
    private String rightMovieTitle;

    private String selectedMovieImdbId;

    private boolean lost;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public String getLeftMovieImdbId() {
        return leftMovieImdbId;
    }

    public void setLeftMovieImdbId(String leftMovieImdbId) {
        this.leftMovieImdbId = leftMovieImdbId;
    }

    public String getRightMovieImdbId() {
        return rightMovieImdbId;
    }

    public void setRightMovieImdbId(String rightMovieImdbId) {
        this.rightMovieImdbId = rightMovieImdbId;
    }

    public String getSelectedMovieImdbId() {
        return selectedMovieImdbId;
    }

    public void setSelectedMovieImdbId(String selectedMovieImdbId) {
        this.selectedMovieImdbId = selectedMovieImdbId;
    }

    public Boolean getLost() {
        return lost;
    }

    public void setLost(Boolean lost) {
        this.lost = lost;
    }

    public String getLeftMovieTitle() {
        return leftMovieTitle;
    }

    public void setLeftMovieTitle(String leftMovieTitle) {
        this.leftMovieTitle = leftMovieTitle;
    }

    public String getRightMovieTitle() {
        return rightMovieTitle;
    }

    public void setRightMovieTitle(String rightMovieTitle) {
        this.rightMovieTitle = rightMovieTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Round round = (Round) o;
        if (!Objects.equals(leftMovieImdbId, round.leftMovieImdbId)) {
            if ((!Objects.equals(leftMovieImdbId, round.rightMovieImdbId))) {
                return false;
            }
            return Objects.equals(rightMovieImdbId, round.leftMovieImdbId);
        } else {
            return Objects.equals(rightMovieImdbId, round.rightMovieImdbId);
        }
    }

    @Override
    public int hashCode() {
        int result = leftMovieImdbId != null ? leftMovieImdbId.hashCode() : 0;
        result = result + (rightMovieImdbId != null ? rightMovieImdbId.hashCode() : 0);
        return result;
    }
}
