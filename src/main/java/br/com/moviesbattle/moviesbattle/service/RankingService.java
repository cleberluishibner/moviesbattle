package br.com.moviesbattle.moviesbattle.service;

import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.Ranking;
import br.com.moviesbattle.moviesbattle.entity.Round;
import br.com.moviesbattle.moviesbattle.entity.User;
import br.com.moviesbattle.moviesbattle.repository.RankingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class RankingService {

    private final RankingRepository rankingRepository;

    public RankingService(final RankingRepository rankingRepository) {
        this.rankingRepository = rankingRepository;
    }

    @Transactional(readOnly = true)
    public Set<Ranking> findAllRanking() {
        return rankingRepository.findAllByOrderByScoreAsc();
    }

    @Transactional
    public void createRanking(final Game game) {
        final Long totalLost = game.getRounds()
                .stream()
                .filter(Round::getLost)
                .count();
        final var score = calculateScore(game.getRounds().size(),
                totalLost.intValue());

        rankingRepository.save(createRanking(game.getUser(), score));
    }

    private Ranking createRanking(final User user, final BigDecimal score) {
        final var ranking = new Ranking();
        ranking.setScore(score);
        ranking.setUser(user);
        return ranking;
    }

    private BigDecimal calculateScore(final Integer totalAnswers,
                                      final Integer totalLost) {
        final var winRate = getWinRate(totalAnswers, totalLost);
        return new BigDecimal(totalAnswers).multiply(winRate);
    }

    private BigDecimal getWinRate(final Integer totalAnswers,
                                  final Integer totalLost) {
        if (totalLost.compareTo(0) == 0) {
            return BigDecimal.ONE;
        }

        return new BigDecimal((double) totalLost / (double) totalAnswers);
    }
}
