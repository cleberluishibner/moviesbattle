package br.com.moviesbattle.moviesbattle.service;

import br.com.moviesbattle.moviesbattle.entity.Round;
import br.com.moviesbattle.moviesbattle.exception.MoviesBattleException;
import br.com.moviesbattle.moviesbattle.gateway.OmdbApiGateway;
import br.com.moviesbattle.moviesbattle.gateway.dto.MovieOmdb;
import br.com.moviesbattle.moviesbattle.gateway.dto.ResultOmdb;
import br.com.moviesbattle.moviesbattle.gateway.dto.SearchMovieOmdb;
import br.com.moviesbattle.moviesbattle.message.Message;
import br.com.moviesbattle.moviesbattle.repository.RoundRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

@Service
public class RoundService {

    private final RoundRepository roundRepository;
    private final OmdbApiGateway gateway;
    private final GameService gameService;

    public RoundService(final RoundRepository roundRepository,
                        final OmdbApiGateway gateway,
                        final GameService gameService) {
        this.roundRepository = roundRepository;
        this.gateway = gateway;
        this.gameService = gameService;
    }

    @Transactional
    public Round getRound() {
        final var game = gameService.getGameUserLoggedOrThrowUserHasNoGame();
        validateIfHasLife(game.getRounds());
        final var round = getCurrentRound(game.getRounds())
                .orElseGet(() -> getNewRound(game.getRounds()));
        round.setGame(game);
        return roundRepository.save(round);
    }

    private void validateIfHasLife(final Set<Round> rounds) {
        long life = 3 - rounds.stream().filter(Round::getLost).count();
        if (life == 0) {
            throw new MoviesBattleException(Message.YOU_LOST);
        }
    }

    private Optional<Round> getCurrentRound(final Set<Round> rounds) {
        return rounds
                .stream()
                .filter(round -> round.getSelectedMovieImdbId() == null)
                .findFirst();
    }

    private Round getNewRound(final Set<Round> rounds) {
        final var round = new Round();
        final int sizeRounds = rounds.size();
        while (sizeRounds == rounds.size()) {
            setLeftMovie(round);
            setRightMovie(round);
            rounds.add(round);
        }
        return round;
    }

    private void setLeftMovie(final Round round) {
        final var leftMovie = getRandomSearchMovieOmdb();
        round.setLeftMovieImdbId(leftMovie.getImdbID());
        round.setLeftMovieTitle(leftMovie.getTitle());
    }

    private void setRightMovie(final Round round) {
        final var rightMovie = getRandomSearchMovieOmdb();
        round.setRightMovieImdbId(rightMovie.getImdbID());
        round.setRightMovieTitle(rightMovie.getTitle());
    }

    private SearchMovieOmdb getRandomSearchMovieOmdb() {
        final var title = Arrays.asList("100", "city", "hero", "movie").get(new Random().nextInt(3));
        final var page = new Random().nextInt(1, 11);
        final ResultOmdb resultOmdb = gateway.search(title, page);
        return resultOmdb.getMovies().get(new Random().nextInt(10));
    }

    @Transactional
    public Boolean answer(final String selectedMovieImdbId) {
        final var game = gameService.getGameUserLoggedOrThrowUserHasNoGame();
        final var currentRound = getCurrentRound(game.getRounds())
                .orElseThrow(() -> new MoviesBattleException(Message.CANT_FIND_CURRENT_ROUND));
        currentRound.setSelectedMovieImdbId(selectedMovieImdbId);

        validateIfSelectedMovieExistInRound(selectedMovieImdbId,
                currentRound.getLeftMovieImdbId(),
                currentRound.getRightMovieImdbId());

        final var selectedIsLoser = selectedIsLoser(
                currentRound.getSelectedMovieImdbId(),
                currentRound.getLeftMovieImdbId(),
                currentRound.getRightMovieImdbId());
        currentRound.setLost(selectedIsLoser);
        roundRepository.save(currentRound);
        return !selectedIsLoser;
    }

    private Boolean selectedIsLoser(final String selectedMovieImdbId,
                                    final String leftMovieImdbId,
                                    final String rightMovieImdbId) {

        final var againstMovieImdbId = selectedMovieImdbId.equals(leftMovieImdbId) ? rightMovieImdbId : leftMovieImdbId;
        return calculateSelectedIsLoser(selectedMovieImdbId, againstMovieImdbId);
    }

    private void validateIfSelectedMovieExistInRound(final String selectedMovieImdbId,
                                                     final String leftMovieImdbId,
                                                     final String rightMovieImdbId) {
        if (!selectedMovieExists(selectedMovieImdbId, leftMovieImdbId, rightMovieImdbId)) {
            throw new MoviesBattleException(Message.SELECTED_MOVIE_NOT_EXIST_IN_ROUND);
        }
    }

    private boolean selectedMovieExists(final String selectedMovieImdbId,
                                        final String leftMovieImdbId,
                                        final String rightMovieImdbId) {
        return selectedMovieImdbId.equals(leftMovieImdbId) || selectedMovieImdbId.equals(rightMovieImdbId);
    }

    private Boolean calculateSelectedIsLoser(final String selectedMovieImdbId,
                                             final String againstMovieImdbId) {
        final var selectedMovie = gateway.getByImdbID(selectedMovieImdbId);
        final var againstMovie = gateway.getByImdbID(againstMovieImdbId);
        return calculateGrade(selectedMovie).compareTo(calculateGrade(againstMovie)) < 1;
    }

    private BigDecimal calculateGrade(final MovieOmdb movieOmdb) {
        final var votes = new BigDecimal(movieOmdb.getImdbVotes().replaceAll(",", ""));
        return movieOmdb.getImdbRating().multiply(votes);
    }
}
