package br.com.moviesbattle.moviesbattle.service;

import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.User;
import br.com.moviesbattle.moviesbattle.exception.MoviesBattleException;
import br.com.moviesbattle.moviesbattle.message.Message;
import br.com.moviesbattle.moviesbattle.repository.GameRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GameService {

    private final GameRepository gameRepository;
    private final LoginService loginService;
    private final RankingService rankingService;


    public GameService(final GameRepository gameRepository,
                       final LoginService loginService,
                       final RankingService rankingService) {
        this.gameRepository = gameRepository;
        this.loginService = loginService;
        this.rankingService = rankingService;
    }

    public Game getGameUserLogged() {
        var user = loginService.authenticatedUser();
        return gameRepository.findByUser(user).orElse(null);
    }

    @Transactional
    public void start() {
        var user = loginService.authenticatedUser();
        validateIfUserAlreadyHasGame(user);
        var game = createGame(user);
        gameRepository.save(game);
    }

    private Game createGame(final User user) {
        var game = new Game();
        game.setUser(user);
        return game;
    }

    private Boolean userHasGame(final User user) {
        return gameRepository.findByUser(user).isPresent();
    }

    private void validateIfUserAlreadyHasGame(final User user) {
        if (userHasGame(user)) {
            throw new MoviesBattleException(Message.USER_ALREADY_HAS_GAME_STARTED);
        }
    }

    @Transactional
    public void finish() {
        var game = getGameUserLoggedOrThrowUserHasNoGame();
        rankingService.createRanking(game);
        gameRepository.delete(game);
    }

    protected Game getGameUserLoggedOrThrowUserHasNoGame() {
        var user = loginService.authenticatedUser();
        return gameRepository.findByUser(user)
                .orElseThrow(() -> new MoviesBattleException(Message.USER_HAS_NO_GAME_STARTED));
    }
}
