package br.com.moviesbattle.moviesbattle.service;

import br.com.moviesbattle.moviesbattle.entity.User;
import br.com.moviesbattle.moviesbattle.exception.MoviesBattleException;
import br.com.moviesbattle.moviesbattle.message.Message;
import br.com.moviesbattle.moviesbattle.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoderParameters;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class LoginService {

    private final UserRepository userRepository;
    private final JwtEncoder encoder;

    public LoginService(final UserRepository userRepository,
                        final JwtEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Transactional(readOnly = true)
    public String login(final User user) {
        final Optional<User> optionalUser = userRepository.findByLoginAndPassword(
                user.getLogin(),
                user.getPassword());

        final User currentUser = optionalUser
                .orElseThrow(() -> new MoviesBattleException(Message.USER_OR_PASSWORD_INVALID));
        return "Bearer " + generateToken(currentUser);
    }

    private String generateToken(final User currentUser) {
        final JwtClaimsSet claims = JwtClaimsSet.builder()
                .subject(currentUser.getLogin())
                .build();
        return this.encoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    protected User authenticatedUser() {
        final String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByLogin(login)
                .orElseThrow(() -> new MoviesBattleException(Message.UNAUTHENTICATED_USER));
    }
}
