package br.com.moviesbattle.moviesbattle.mapper;

import br.com.moviesbattle.moviesbattle.dto.UserDto;
import br.com.moviesbattle.moviesbattle.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    User toEntity(UserDto userDTO);
}
