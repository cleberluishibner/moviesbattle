package br.com.moviesbattle.moviesbattle.mapper;

import br.com.moviesbattle.moviesbattle.dto.RoundDto;
import br.com.moviesbattle.moviesbattle.entity.Round;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoundMapper {
    RoundDto toDto(Round round);
}
