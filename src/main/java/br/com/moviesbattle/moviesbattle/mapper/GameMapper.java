package br.com.moviesbattle.moviesbattle.mapper;

import br.com.moviesbattle.moviesbattle.dto.GameDto;
import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.Round;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface GameMapper {

    @Mapping(source = "rounds", target = "life")
    GameDto toDto(Game game);

    default Integer getLife(final Set<Round> rounds) {
        return 3 - Math.toIntExact(rounds.stream().filter(Round::getLost).count());
    }
}
