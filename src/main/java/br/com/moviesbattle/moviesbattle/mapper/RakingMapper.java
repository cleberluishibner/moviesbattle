package br.com.moviesbattle.moviesbattle.mapper;

import br.com.moviesbattle.moviesbattle.dto.RankingDto;
import br.com.moviesbattle.moviesbattle.entity.Ranking;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface RakingMapper {

    @Mapping(source = "user.login", target = "login")
    RankingDto toDto(Ranking ranking);

    List<RankingDto> toDto(Set<Ranking> rankings);
}
