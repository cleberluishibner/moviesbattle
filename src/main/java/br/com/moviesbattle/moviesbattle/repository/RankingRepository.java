package br.com.moviesbattle.moviesbattle.repository;

import br.com.moviesbattle.moviesbattle.entity.Ranking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface RankingRepository extends CrudRepository<Ranking, Integer> {

    Set<Ranking> findAllByOrderByScoreAsc();
}
