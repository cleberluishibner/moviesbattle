package br.com.moviesbattle.moviesbattle.repository;

import br.com.moviesbattle.moviesbattle.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findByLoginAndPassword(final String login, final String password);

    Optional<User> findByLogin(final String login);
}
