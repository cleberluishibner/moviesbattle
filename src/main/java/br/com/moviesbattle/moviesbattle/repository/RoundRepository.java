package br.com.moviesbattle.moviesbattle.repository;

import br.com.moviesbattle.moviesbattle.entity.Round;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoundRepository extends CrudRepository<Round, Integer> {

}
