package br.com.moviesbattle.moviesbattle.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = MoviesBattleException.class)
    protected ResponseEntity handleConflict(MoviesBattleException ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}