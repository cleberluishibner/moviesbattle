package br.com.moviesbattle.moviesbattle.exception;

public class MoviesBattleException extends RuntimeException {
    public MoviesBattleException(final String message) {
        super(message);
    }
}
