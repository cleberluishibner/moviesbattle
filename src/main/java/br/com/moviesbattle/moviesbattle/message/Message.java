package br.com.moviesbattle.moviesbattle.message;

public class Message {
    public static final String USER_OR_PASSWORD_INVALID = "Invalid login or password";
    public static final String UNAUTHENTICATED_USER = "unauthenticated user";
    public static final String USER_ALREADY_HAS_GAME_STARTED = "The user already has a game started";
    public static final String USER_HAS_NO_GAME_STARTED = "The user has no game started";
    public static final String CANT_FIND_CURRENT_ROUND = "Cant find current round, please request another";
    public static final String SELECTED_MOVIE_NOT_EXIST_IN_ROUND = "Selected movie does not exist in round";
    public static final String YOU_LOST = "You lost, finish this game and start a new one";
}
