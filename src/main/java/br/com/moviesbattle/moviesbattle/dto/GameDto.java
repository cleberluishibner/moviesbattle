package br.com.moviesbattle.moviesbattle.dto;

public class GameDto {

    private Integer id;
    private Integer life;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLife() {
        return life;
    }

    public void setLife(Integer life) {
        this.life = life;
    }
}
