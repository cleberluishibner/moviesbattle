package br.com.moviesbattle.moviesbattle.dto;

import java.math.BigDecimal;

public class RankingDto {

    private String login;
    private BigDecimal score;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
}
