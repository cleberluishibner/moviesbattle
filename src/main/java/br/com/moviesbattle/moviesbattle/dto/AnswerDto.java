package br.com.moviesbattle.moviesbattle.dto;

import javax.validation.constraints.NotBlank;

public class AnswerDto {
    @NotBlank
    private String selectedMovieImdbId;

    public String getSelectedMovieImdbId() {
        return selectedMovieImdbId;
    }

    public void setSelectedMovieImdbId(String selectedMovieImdbId) {
        this.selectedMovieImdbId = selectedMovieImdbId;
    }
}
