package br.com.moviesbattle.moviesbattle.dto;


public class RoundDto {
    private String leftMovieImdbId;
    private String leftMovieTitle;
    private String rightMovieImdbId;
    private String rightMovieTitle;

    public String getLeftMovieImdbId() {
        return leftMovieImdbId;
    }

    public void setLeftMovieImdbId(String leftMovieImdbId) {
        this.leftMovieImdbId = leftMovieImdbId;
    }

    public String getLeftMovieTitle() {
        return leftMovieTitle;
    }

    public void setLeftMovieTitle(String leftMovieTitle) {
        this.leftMovieTitle = leftMovieTitle;
    }

    public String getRightMovieImdbId() {
        return rightMovieImdbId;
    }

    public void setRightMovieImdbId(String rightMovieImdbId) {
        this.rightMovieImdbId = rightMovieImdbId;
    }

    public String getRightMovieTitle() {
        return rightMovieTitle;
    }

    public void setRightMovieTitle(String rightMovieTitle) {
        this.rightMovieTitle = rightMovieTitle;
    }
}
