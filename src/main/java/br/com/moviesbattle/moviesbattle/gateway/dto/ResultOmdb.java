package br.com.moviesbattle.moviesbattle.gateway.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ResultOmdb {
    @JsonProperty("Search")
    private List<SearchMovieOmdb> movies;
    private Integer totalResults;
    @JsonProperty("Response")
    private String response;

    public List<SearchMovieOmdb> getMovies() {
        return movies;
    }

    public void setMovies(List<SearchMovieOmdb> movies) {
        this.movies = movies;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
