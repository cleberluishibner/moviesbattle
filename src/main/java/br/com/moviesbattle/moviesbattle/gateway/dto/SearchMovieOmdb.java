package br.com.moviesbattle.moviesbattle.gateway.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchMovieOmdb {
    private String imdbID;
    @JsonProperty("Title")
    private String title;

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
