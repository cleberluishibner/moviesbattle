package br.com.moviesbattle.moviesbattle.gateway.dto;

import java.util.List;

public class SearchOmdb {
    private List<SearchMovieOmdb> movies;

    public List<SearchMovieOmdb> getMovies() {
        return movies;
    }

    public void setMovies(List<SearchMovieOmdb> movies) {
        this.movies = movies;
    }
}
