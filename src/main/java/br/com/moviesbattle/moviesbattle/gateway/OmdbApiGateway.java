package br.com.moviesbattle.moviesbattle.gateway;

import br.com.moviesbattle.moviesbattle.gateway.dto.MovieOmdb;
import br.com.moviesbattle.moviesbattle.gateway.dto.ResultOmdb;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "OmdbApi", url = "${omdbapi.url}${omdbapi.key}")
public interface OmdbApiGateway {

    @GetMapping
    ResultOmdb search(@RequestParam("s") String title, @RequestParam("page") Integer page);

    @GetMapping
    MovieOmdb getByImdbID(@RequestParam("i") String imdbID);
}
