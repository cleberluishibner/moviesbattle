package br.com.moviesbattle.moviesbattle.service;

import br.com.moviesbattle.moviesbattle.builder.GameBuilder;
import br.com.moviesbattle.moviesbattle.builder.RoundBuilder;
import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.Round;
import br.com.moviesbattle.moviesbattle.gateway.OmdbApiGateway;
import br.com.moviesbattle.moviesbattle.gateway.dto.MovieOmdb;
import br.com.moviesbattle.moviesbattle.repository.RoundRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RoundServiceTest {

    @Mock
    private RoundRepository roundRepository;
    @Mock
    private OmdbApiGateway gateway;
    @Mock
    private GameService gameService;

    @InjectMocks
    private RoundService roundService;

    @Test
    public void whenGetRoundAsTheGameAlreadyStartedShouldSucceedReturnCurrentRound() {
        final Round expectedRound = new RoundBuilder()
                .random()
                .noSelectedMovieImdbId()
                .build();

        final Round finishedRound = new RoundBuilder()
                .random()
                .build();

        final Game game = new GameBuilder()
                .random()
                .setRound(Set.of(expectedRound, finishedRound))
                .build();

        Mockito.when(gameService.getGameUserLoggedOrThrowUserHasNoGame()).thenReturn(game);
        Mockito.when(roundRepository.save(expectedRound)).thenReturn(expectedRound);
        final var currentRound = roundService.getRound();

        assertEquals(expectedRound, currentRound);
    }

    @Test
    void whenAnswerSelectedWinnerMovie() {
        final Round currentRound = new RoundBuilder()
                .random()
                .noSelectedMovieImdbId()
                .build();

        final Round finishedRound = new RoundBuilder()
                .random()
                .build();

        final Game game = new GameBuilder()
                .random()
                .setRound(Set.of(currentRound, finishedRound))
                .build();
        final MovieOmdb selectedMovieOmdb = new MovieOmdb();
        selectedMovieOmdb.setImdbRating(BigDecimal.TEN);
        selectedMovieOmdb.setImdbVotes("100");

        final MovieOmdb againstMovieOmdb = new MovieOmdb();
        againstMovieOmdb.setImdbRating(BigDecimal.ONE);
        againstMovieOmdb.setImdbVotes("90");

        Mockito.when(gameService.getGameUserLoggedOrThrowUserHasNoGame()).thenReturn(game);
        Mockito.when(gateway.getByImdbID(currentRound.getLeftMovieImdbId())).thenReturn(selectedMovieOmdb);
        Mockito.when(gateway.getByImdbID(currentRound.getRightMovieImdbId())).thenReturn(againstMovieOmdb);

        final Boolean answer = roundService.answer(currentRound.getLeftMovieImdbId());
        assertTrue(answer);
    }

    @Test
    void whenAnswerSelectedLoserMovie() {
        final Round currentRound = new RoundBuilder()
                .random()
                .noSelectedMovieImdbId()
                .build();

        final Round finishedRound = new RoundBuilder()
                .random()
                .build();

        final Game game = new GameBuilder()
                .random()
                .setRound(Set.of(currentRound, finishedRound))
                .build();

        final MovieOmdb selectedMovieOmdb = new MovieOmdb();
        selectedMovieOmdb.setImdbRating(BigDecimal.TEN);
        selectedMovieOmdb.setImdbVotes("90");

        final MovieOmdb againstMovieOmdb = new MovieOmdb();
        againstMovieOmdb.setImdbRating(BigDecimal.TEN);
        againstMovieOmdb.setImdbVotes("100");

        Mockito.when(gameService.getGameUserLoggedOrThrowUserHasNoGame()).thenReturn(game);
        Mockito.when(gateway.getByImdbID(currentRound.getLeftMovieImdbId())).thenReturn(selectedMovieOmdb);
        Mockito.when(gateway.getByImdbID(currentRound.getRightMovieImdbId())).thenReturn(againstMovieOmdb);

        final Boolean answer = roundService.answer(currentRound.getLeftMovieImdbId());
        assertFalse(answer);
    }
}