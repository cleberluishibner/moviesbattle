package br.com.moviesbattle.moviesbattle.builder;

import br.com.moviesbattle.moviesbattle.entity.User;

import java.util.Random;
import java.util.UUID;

public class UserBuilder {

    private Integer id;
    private String login;
    private String password;

    public UserBuilder random() {
        id = new Random().nextInt();
        login = "login " + UUID.randomUUID();
        password = "password " + UUID.randomUUID();
        return this;
    }

    public User build() {
        final var user = new User();
        user.setId(this.id);
        user.setLogin(this.login);
        user.setPassword(password);
        return user;
    }
}
