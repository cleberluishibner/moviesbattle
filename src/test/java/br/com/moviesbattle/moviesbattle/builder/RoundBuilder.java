package br.com.moviesbattle.moviesbattle.builder;

import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.Round;

import java.util.Random;
import java.util.UUID;

public class RoundBuilder {

    private Integer id;
    private Game game;
    private String leftMovieImdbId;
    private String leftMovieTitle;
    private String rightMovieImdbId;
    private String rightMovieTitle;
    private String selectedMovieImdbId;
    private Boolean lost;

    public RoundBuilder setGame(final Game game){
        this.game = game;
        return this;
    }

    public RoundBuilder noSelectedMovieImdbId(){
        this.selectedMovieImdbId = null;
        return this;
    }

    public RoundBuilder random() {
        this.id = new Random().nextInt();
        this.leftMovieImdbId = "leftMovieImdbId " + UUID.randomUUID();
        this.leftMovieTitle = "leftMovieTitle " + UUID.randomUUID();
        this.rightMovieImdbId = "rightMovieImdbId " + UUID.randomUUID();
        this.rightMovieTitle = "rightMovieTitle " + UUID.randomUUID();
        this.selectedMovieImdbId = "selectedMovieImdbId " + UUID.randomUUID();
        this.lost = Boolean.FALSE;
        return this;
    }

    public Round build() {
        final var round = new Round();
        round.setId(this.id);
        round.setGame(this.game);
        round.setLeftMovieImdbId(this.leftMovieImdbId);
        round.setLeftMovieTitle(this.leftMovieTitle);
        round.setRightMovieImdbId(this.rightMovieImdbId);
        round.setRightMovieTitle(this.rightMovieTitle);
        round.setSelectedMovieImdbId(this.selectedMovieImdbId);
        round.setLost(this.lost);
        return round;
    }
}
