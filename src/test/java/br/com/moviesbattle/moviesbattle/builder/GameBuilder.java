package br.com.moviesbattle.moviesbattle.builder;

import br.com.moviesbattle.moviesbattle.entity.Game;
import br.com.moviesbattle.moviesbattle.entity.Round;
import br.com.moviesbattle.moviesbattle.entity.User;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class GameBuilder {

    private Integer id;
    private Set<Round> rounds = new LinkedHashSet<>();
    private User user;

    public GameBuilder random() {
        this.id = new Random().nextInt();
        this.user = new UserBuilder().random().build();
        this.rounds.add( new RoundBuilder().random().build());
        return this;
    }

    public GameBuilder setRound(Set<Round> rounds){
        this.rounds = rounds;
        return this;
    }

    public Game build() {
        var game = new Game();
        game.setRounds(this.rounds);
        game.setId(this.id);
        game.setUser(this.user);
        return game;
    }
}
